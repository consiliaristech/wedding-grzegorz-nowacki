@extends('layouts.app')

@section('content')
    <div class="row justify-content-md-center">
        <table class="table col-4">
            <thead class="thead-dark">
            <tr>
                <th scope="col" colspan="2" class="text-center">Lead information</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Id</th>
                <td>{{$lead->id}}</td>
            </tr>
            <tr>
                <th scope="row">Name</th>
                <td>{{$lead->name}}</td>
            </tr>
            <tr>
                <th scope="row">Surname</th>
                <td>{{$lead->surname}}</td>
            </tr>
            <tr>
                <th scope="row">Phone</th>
                <td>{{$lead->phone}}</td>
            </tr>
            <tr>
                <th scope="row">Email</th>
                <td>{{$lead->email}}</td>
            </tr>
            <tr>
                <th scope="row">Message</th>
                <td>{{$lead->msg}}</td>
            </tr>
            <tr>
                <th scope="row">Receive date</th>
                <td>{{$lead->created_at}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="row justify-content-md-center mt-5">
        <a class="btn btn-primary" href="{{url('/leads/list')}}" role="button">Back to the leads list</a>
    </div>
@endsection
