@extends('layout')

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-4">
            <h1>Add New Lead</h1>
            <hr>
            <form action="/leads" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="title">Surname</label>
                    <input type="text" class="form-control" id="surname" name="surname" value="{{ old('surname') }}">
                    @if ($errors->has('surname'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('surname') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="title">Mobile phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                    @if ($errors->has('phone'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('phone') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="title">E-mail</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="description">Additional informations</label>
                    <textarea type="text" class="form-control" id="msg" name="msg" value="{{ old('msg') }}"></textarea>
                    @if ($errors->has('msg'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('msg') }}
                        </div>
                    @endif
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
