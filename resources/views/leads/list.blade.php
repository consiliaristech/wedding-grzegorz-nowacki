@extends('layouts.app')

@section('content')
    <table class="table table-hover">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Surname</th>
            <th scope="col">Phone</th>
            <th scope="col">Email</th>
            <th scope="col">Message</th>
            <th scope="col">Receive date</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leads as $lead)
            <tr onclick="window.location='{{url('/leads/' . $lead->id)}}'" style="cursor:pointer;">
                <th scope="row">{{$lead->id}}</th>
                <td>{{$lead->name}}</td>
                <td>{{$lead->surname}}</td>
                <td>{{$lead->phone}}</td>
                <td>{{$lead->email}}</td>
                <td>{{$lead->msg}}</td>
                <td>{{$lead->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
