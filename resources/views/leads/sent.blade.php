@extends('layout')

@section('content')
    <div class="flex-center position-ref full-height text-center">
        <div class="col-4">
            <div class="alert alert-success" role="alert">
                <h3 class="alert-heading">Well done!</h3>
                <p>Your information was sent successfuly.</p>
            </div>
        </div>
    </div>
@endsection
