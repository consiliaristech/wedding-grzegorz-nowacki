<?php

namespace App\Console\Commands;

use App\Lead;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class LeadStatistics extends Command
{

    private const OPTIONS = [
        'Day' => [
            'param' => 'd',
            'format' => 'd-M-Y'
        ],
        'Month' => [
            'param' => 'M',
            'format' => 'M-Y'
        ]
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:statistics
                            {groupBy=Day : Available options is a Day or Month}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get statistics about the amount of sent leads by grouping them by months or days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Lead $lead)
    {
        $option = ucfirst(strtolower($this->argument('groupBy')));

        $groupBy = self::OPTIONS[$option] ?? null;
        if (!$groupBy) {
            $this->error('Given grouping options is not allowed.');
            return;
        }

        $leads = Lead::all(['id', 'name', 'surname', 'phone', 'email', 'msg', 'created_at'])
            ->groupBy(function ($date) use ($groupBy) {
                return Carbon::parse($date->created_at)->format($groupBy['param']);
            })->toArray();

        $grouped = array_map(
            function ($value) use ($groupBy) {
                $date = current($value)['created_at'];
                $day = Carbon::parse($date)->format($groupBy['format']);
                return [$day, count($value)];
            },
            $leads
        );

        $headers = [$option, 'Count'];

        $this->table($headers, $grouped);
    }
}
