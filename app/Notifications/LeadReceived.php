<?php

namespace App\Notifications;

use App\Lead;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LeadReceived extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Lead
     */
    private $lead;

    /**
     * Create a new lead instance.
     *
     * @param Lead $lead
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Determine which queues should be used for each lead channel.
     *
     * @return array
     */
    public function viaQueues()
    {
        return [
            'mail' => 'mail-queue',
        ];
    }

    /**
     * Get the lead's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the lead.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/leads/' . $this->lead->id);
        $mailMsg = (new MailMessage)
            ->subject('A new lead has been sent.');
        foreach (array_intersect_key($this->lead->toArray(), array_flip(['name', 'surname', 'phone', 'email', 'msg'])) as $key => $value) {
            $mailMsg->line("$key: $value");
        }
        $mailMsg->action('View Lead', $url);

        return $mailMsg;
    }

    /**
     * Get the array representation of the lead.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
