<?php


namespace App\Repository;


use App\Lead;
use App\Notifications\LeadReceived;
use App\User;
use Illuminate\Support\Facades\Notification;

class LeadRepository
{
    public function add(array $data)
    {
        $model = new Lead();
        $model->fill($data);
        $model->save();

        $users = User::all();
        Notification::send($users, new LeadReceived($model->refresh()));
    }
}
