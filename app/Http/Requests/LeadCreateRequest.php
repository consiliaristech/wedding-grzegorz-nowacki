<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'min:2',
                'max:25'
            ],
            'surname' => [
                'required',
                'string',
                'min:2',
                'max:25'
            ],
            'phone' => [
                'required',
                'regex:/^\+[0-9]{2} ?([0-9]{3} ?){3}$/'
            ],
            'email' => [
                'required',
                'string',
                'email'
            ],
            'msg' => 'sometimes'
        ];
    }
}
