<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadCreateRequest;
use App\Lead;
use App\Notifications\LeadReceived;
use App\Repository\LeadRepository;


class LeadController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('leads.create');
    }

    /**
     * @param LeadCreateRequest $request
     * @param LeadRepository $repository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(LeadCreateRequest $request, LeadRepository $repository)
    {
        $repository->add($request->validated());
        return view('leads.sent');
    }

    public function list(){
        return view('leads.list', ['leads' => Lead::all()->sortByDesc('created_at')]);
    }

    public function show(Lead $lead)
    {
        return view('leads.lead', ['lead' =>$lead]);
    }

    public function preview(Lead $lead)
    {
        return (new LeadReceived($lead))
            ->toMail($lead);
    }
}
