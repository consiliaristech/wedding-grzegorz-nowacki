# Wedding-Grzegorz-Nowacki

## Requrements

- PHP ^7.1.3
- composer
- npm
- laravel
- MySql

## Instalation
Install and configure application by running following commands

```
composer install
mv .env.example .env
npm install && npm run dev
php artisan key:generate
```

Enter your DB connection (use `DB_CONNECTION=mysql` for MySQL) in .env file and then:
```
php artisan migrate
```

Enter your MAIL (use `MAIL_MAILER=smtp` for testing)configuration in .env file:
```
MAIL_MAILER=smtp
MAIL_HOST=poczta.o2.pl
MAIL_PORT=465
MAIL_USERNAME=youremail@o2.pl
MAIL_PASSWORD=password
MAIL_ENCRYPTION=ssl
MAIL_FROM_ADDRESS=youremail@o2.pl
MAIL_FROM_NAME="${APP_NAME}"
```

Set QUEUE_CONNECTION in .env file to:
```
QUEUE_CONNECTION=database
```

Run queue worker:
```
php artisan queue:work --queue=mail-queue
```

## Test application

Run application in web browser using the link: 
```
[your domain]/leads/list" and register a new user of the admin panel
```

Register a new user of the admin panel.

> **Hint:** Remember to give a real email address so that the lead could be sent properly to your mailbox

Go to the following link:
```
[your domain]/leads
```
Fill in the form and send lead and than check your mailbox.
In the mail you will find a button or link (at the bottom of the message) which will redirect you to the details of the lead within the admin panel.

## Command for leads statistics

A new command for leads statistics has been added:
```
php artisan lead:statistics [Day|Month]
```

You can group them by day or month. Grouping option name is not case sensitive.
